# IoT & Blockchain

## General

### [Blockchain for the IoT: Opportunities and Challenges](https://arxiv.org/pdf/1805.02818.pdf)

**Abstract**

_Blockchain alapú technológiák terjedése átalakulásokat okozott a pénzügyi
szektorban, megjelentek a kriptovaluták. Az alapvető koncepciók, mint a decentralizált bizalom, és elosztott ledger
bíztató elképzelések az IoT alkalmazásokhoz. Eddig, azonban nem terjedt el ezeknek a
technológiáknak a használata a pénzügy doménjén kívül, ami a Blockchain technikák alkalmazásával járó
architektúrális nehézségeknek köszönhető. **Ez a tanulmány a Blockchain által szolgáltatot szerkezeti
előnyöket vizsgálja, melyek megoldást jelenthetnek az IoT területén tervezési nehézségeket okozó problémákra.**_

------

#### Internet of Things layers

![layers](https://github.com/Mrpatekful/iot-projects/blob/master/assets/iot_overview.png)

- **End-device Layer**

Alacsony energiaszükségletű szenzorok, melyek kevés számítási és tárolási teljesítménnyel bírnak.
Errőforrások szempontjból a legnagyobb kényszerekkel rendelkező szint. **Feladatuk a környezetükről
adatokat szolgáltatni, leggyakrabban valamilyen vezeték nélküli kapcsolaton kereszetül a felsőbb rétegeknek.**

- **Edge-device Layer**

**Feladata, hogy hálózati kapcsolatot biztosítson és szenzor adatot gyűjtsön az end-device layertől.** A továbbítás
előtt adatok előfeldolgozása is itt történik, így ez a réteg már rendelkezik számítási és tárolási kapacitással is.

- **Server or backend layer**

Az IoT alkalmazások felhasználói ezzel a rétegben vannak kapcsolatban. **Hozzáférést biztosít az infrastruktúra
vizsgálatához és irányításához web-szervereken, adat-analitikai eszközökön, illetve adatbázisokon keresztül.**
Ezen a szinten lévő eszközök rendelkeznek a legnagyobb tárolási kapacitással és számítási teljesítménnyel.


_Ez a jellegű architektúra a legelterjedtebb IoT alkalmazásokban, azonaban Blockchain integrációja egy ilyen rendszerbe
nehézségeket jelenthet._ 

------

#### Blockchain technologies

- **Cryptographic Digital Signature**

Publikus és privát kulcspárt használ a Blockhcain, a tranzakciók hitelesítéséhez. A felhasználók ezzel a digitális
aláírással látják el műveleteiket (privát kulcs), és a hálózat többi szereplője a publikus kulccsal 
ellenőrzi, hogy valóban a megfelelő felhasználó írta alá a tranzakciót. 

- **Distributed Ledger**

Blockchain egy elosztott tárolót használ a tranzakciók tárolásához. Alapvetően, a hálózatban szereplő összes platform
rendelkezik a tranzakciókkal, vagy azoknak egy részével. Egy tranzakció csak akkor kerül eltárolásra, ha a szereplők
megegyeznek annak hitelességéről (consesus algoritmus). Ez gyakorlatilag immúnissá teszi a Blockchaint nem kívánatos
változtatások ellen.

- **Consensus algorithm**

Blockchain egy peer-to-peer megegyezési stratégia segítségével végzi a hitelesítést, 
melyet consesus protocolnak neveznek.

------

#### Lehetőségek

- **Privacy/anonymity**

Blockchain tranzakciói a publikus kulcs és hash által generált identitást használják. Ez anonimitást
biztosíthat olyan IoT alkalmazásoknak, melyek érzékeny információval bírnak, hogy elrejtsék valós identitásukat a 
hálózatban.

- **Monetary exchange of data and compute**

IoT alkalmazások okos városokban szenzorok és lakosok segítségével digitális szolgáltatásokat nyújthatnak más lakosoknak,
melyekhez központi fontosságú lehet, hogy pénzügyi vagy egyéb jutalmakkal ösztönözzéka az embereket ezekben az
rendszerekben a részvételre. 

- **Record transactions for account and audit**

Az adat az IoT alkalmazásokból több infrastuktúra rétegen halad keresztül, melyeket több különböző szervezet birtokol.
Ellátási lánc felügyelés feladata, hogy ellenőrizze az adat és tőke áramlását az ellátás folyamatán keresztül.
Blockchain használata ezeknek a tényezőknek a mozgatására növeli a megbízhatóságot a szereplőkkel szemben.

- **Smart Contracts**

Smart contract egy digitális szerződés, amely rendszerbe ágyazva, meghatározott feltelek mellett hajtódik
végre. Tranzakciók automatikus elvégzésére alkalmas, mely IoT területen felhasználató szenzor adatok különböző
résztvevők közti automatikus mozgatására és eladására.


------


### [A Hypergraph-Based Blockchain Model and Application in Internet of Things-Enabled Smart Homes](https://www.researchgate.net/publication/327217691_A_Hypergraph-Based_Blockchain_Model_and_Application_in_Internet_of_Things-Enabled_Smart_Homes)

**Abstract**

_Az IoT technológia terjedésével több milliárd okos eszköz kapcsolódik egymáshoz, mely nagy biztonsági
réseket hagy a támadóknak. Egy jó megoldást jelent a blockchain, mely azonban nagy számítás igányt és
tárkapacitást igényel az azt használó eszközöktől. Tipikusan a smart homeokban megtalálható alkalmazások és végpontok, azonban
nem rendelkeznek elegendő erőforrással. **Ez a tanulmány egy hipergráf alapú tehermentesítést 
ír le, ezeknek a problémáknak a megoldására.**_

------

![hyper](https://github.com/Mrpatekful/iot-projects/blob/master/assets/hyper.png)

------

### [Blockchain in Internet of Things: Challenges and Solutions](https://arxiv.org/pdf/1608.05187.pdf)

**Abstract**

_Az IoT eszközök száma exponenciális növekedést mutat a kutatási és ipari területeken, 
azonban még mindig biztonsági problémákkal küzködik. Hagyományos biztonsági megoldások 
nem alkalmazhatóak a decentralizált topológia, valamint alacsony erőforrásal bíró eszközök miatt.
**Ez a tanulmány egy hipergráf alapú tehermentesítést ír le, ezeknek a problémáknak a megoldására.**_

------

#### Block-based IoT Architecture

##### Smart home

Három részből épül fel:

- Eszközök: az összes IoT eszköz az otthonban.
- Lokális BC: Egy biztonságos és privát blockchain, amit egy darab elégséges erőforrásokkal rendelkező ezsköz
 bányász és tárol. Egy példa lehetne egy smart HUB vagy egy asztali számítógép.
A lokális BC egy policy headerrel rendelkezik, mely egy elérés irányítás lista, ami 
lehetővé teszi a tulajdonosnak, hogy irányítsa az összes tranzakciót.
- Lokális tároló: Az otthonban lehetséges egy tároló fenntartása, mely akár egy backup lehetőségként is szolgálhat.


![structure](https://github.com/Mrpatekful/iot-projects/blob/master/assets/h_arch_iot.png)

------

### [On blockchain and its integration with IoT. Challenges and opportunities](https://reader.elsevier.com/reader/s9+d/pii/S0167739X17329205?token=1BF072D08B4DFDDD5F9A3D0749379437C5135B41BD430D019E36FBFF70E7E781B72A991ACCE50B2ED57E3696337F37D5)


**Abstract**

------


### [Designing a blockchain-based IoT infrastructure with Ethereum, Swarm and LoRa *](https://arxiv.org/pdf/1809.07655.pdf)


**Abstract**

_**Ez a tanulmány egy standardizált IoT infrastruktúrát ír le, ahol az adat egy DDOS ellenálló, hibatűrő, 
elosztott tároló eszközön, és az adatelérést egy decentralizált blockchain irányítja. Az
illusztrált rendszer LoRa-t használja, mint feltörekvő hálózat technológia. Swarm az elosztott tár, valamint
Ethereum mint alap platform.**_

------

#### Blockchain elem típusok

- Miner: Minerek speciális node-ok, amik tranzakciókat pakolnak blokkokba és konszenzus algoritmust futtatnak, melyek
rendszerkövetelményeket elégítenek ki, hogy gazdasági előnyt érjenek el.
- Full node: Full node-ok letöltik az egész blockchain-t és igazolják az összes tranzkació integritását, 
ezáltal megbízhatóvá és decentralizáltá téve azt.
- Thin client: Thin kliensek csak azokat a blokk haedereket töltik le, melyek a tranzakciók hashét tartalmazzák magukban.
Így lehetséges interkacióba kerülnia  blockchain-nel minimális tárolási és számítási kapacitással is.
- Server-Trusting Client: Bitcoin Client API, mely könnyű, kicsi és biztonságos klienseket készít, 
magas erőforrás kényzerekkel rendelkező rendszerekhez.

![int](https://github.com/Mrpatekful/iot-projects/blob/master/assets/int.png)

![lora](https://github.com/Mrpatekful/iot-projects/blob/master/assets/lora.png)


[Bether project](https://github.com/kozyilmaz/bether)

------

### [Hybrid-IoT: Hybrid Blockchain Architecture for Internet of Things - PoWSub-blockchains](https://arxiv.org/abs/1804.03903)


**Abstract**

IoT egy decentralizáltan együttműködő okoseszközök rendszerévé fejlődött. Azonban
jelenlegi IoT platformok megoldásai centralizált felhő alapú infrastruktúrát használnak a
a magas számításés tárolási kapacitás igényű feladatok ellátására. Blockchain integrálása elérhetővé teszi a teljesen 
elosztott és biztonságos konszenzus alapú rendszerek kiépítését. _**Ebben a tanulmányban egy hibrit rendszert mutatnak be.
Hybrid IoT-ban az eszközök részegységenként alkotnak PoW (Proof of Work) alapú blockchaineket. A részegységek közt
egy BFT inter-connector framework biztosít kapcsolatot.**_

------


### [Delay and Communication Tradeoffs for Blockchain Systems with Lightweight IoT Clients](https://arxiv.org/pdf/1807.07422.pdf)


------


## Security

### [IoTChain: A Three-Tier Blockchain-based IoT Security Architecture](https://arxiv.org/pdf/1806.02008.pdf)


------


### [CIoTA: Collaborative IoT Anomaly Detection via Blockchain](https://arxiv.org/pdf/1803.03807.pdf)

**Abstract**

_Az IoT eszközök gyors terjedésük miatt előbb vagy utóbb központi részét fogják képezni mindennapjainknak. 
Ez, azonban az IoT eszközök sebezhetőségeinek növekedésével is jár. Az ilyen rendszereket érő támadások ellen,
nem-felügyelt módszerekkel, mint például anomália detekciós modellel tudunk védekezni. Ezek a modellek, 
azonban érzékenyek adverzáliális támadásokra, mivel a bármilyen rendszerben szereplő tényező lehet ellenség.
**Ebben a papírban a szerzők bemutatják a CIoTA frameworkot, mely blockchain segítségével végez elosztott, 
kollaboratív anomália detekciót.**_

------

#### Intrusion detection system

1) **Adverzáliális támadás**

Tanulási fázis alatt az anomália detekciós rendszer a komponensek normális viselkedését tanulja, 
így ha egy kártékony szereplő is részt vesz a folyamatban, az teljesen rejtett marad a detekciós fázis alatt.

2) **Hamis pozitív detekció**

Sok olyan eset van, amikor a tanulási fázis nem fedi le teljesen az eszközök viselkedését, például
egy mozgás érzékelő kameránál egy eddig nem tapasztalt felvétel. Ezeket az anomália detekció tévesen kártékony
jelenségnek fogja detektálni.

#### CIoTA

A CIoTA rendszer arra a feltételezésre épít, hogy egy rendszerben sok azonos típusú modell 
egyszerre kezdi el egy lokális anomália detekciós modell tanulását. Ebben az esetben valószínűtlen,
hogy az IoT eszközök túlnyomó része, egy támadó által kihasznát komponens legyen a rendszerben.

A tanulmányban használt anomália detekciós model egy EMM (extensible Markov model).

Ahhoz, hogy a modellbe egy új belső koncepciót olvasszon a rendszer, a kompomenseknek konszunzusra kell értkeznie
a beillesztendő állapot megbízhatóságát illetően.


------

### [LSB: A Lightweight Scalable BlockChain for IoT Security and Privacy](https://arxiv.org/pdf/1712.02969.pdf)

**Abstract**

_Blockchain nagy figyelmet kapott az elmúlt időben, a technológia nyújtotta biztonsági előnyök miatt.
A biztonségért, azonban nagy számítási költségeket vet fel a Blochain, és erős kényszereket tesz az azt használó
eszközökre. Ez az overhead okozta késleltetés nem ideális egy IoT környezetben. **Erre a problémára
szolgáltat megoldást a Lightweight Scalable Blochain (LSB), melyet a cikk írói egy smart home
környezetében mutatnak be.**_

------

#### Baseline

- **Erőforrás felhasználás**

Jelenlegi IoT rendszerek limitált erőforrásokkal rendelkeznek, sávszélesség, számítási teljesítmény, valmamint
memóriát illetően, ami inkompatibilis a biztonsági megoldások komplexitását kielégítő követelményekkel.

- **Centralizáció**

IoT ökoszisztémák egy központilag vezérelt komminukációs modellre alapoznak, ahol minden eszköz egy vezérlő
által van összekapcsolva és autentikálva.

- **Titkosítás hiánya**

Számos IoT alkalmazás pontos adatoknak a felfedését igényli a Service Providerek felé, hogy személyre szabott
szolgáltatásokat vehessenek igénybe.

#### LSB

Egy smart home IoT rendszerben az alacsony erőforrású eszközök nagy előnyben részesülnek egy
központi manager-től, mely osztott kulcsú kommunikációval bonyolítja le a ki- és bemenő kéréseket.
LSB decentralizációt egy fedő hálózattal éri el, ahol a magas erőforrású eszközök együttesen vezérelnek egy 
publikus blockchaint, mely end-to-end biztonságot eredményez a résztvevők között. A blockchain
sebességének optimalizálásra több technikát is alkalmaz a framework, mint a lightweight consensus és
az elosztott trust, valamint továbbítás vezérlés.

**Overlay**

Az overlay hálózat potenciálisan nagy számú eszközökből állhat, így a skálázhatóság megőrzéséért a publikus
blockchain-t az overlay hálózatban résztvevő csomópontok egy részhalmaza kezeli. Ezt a részhalmazot Cluster Head (HD)
csomópontok alkotják, melyek valamilyen clusterezési algoritmussal meghatározott központi elemek, és a saját
clusterjükben szereplő csomópontokért felelősek. A CH-kat az alábbi illusztráción OBM-nek jelölik,
ami az Overay Block Manager-re utal.

![lsb](https://github.com/Mrpatekful/iot-projects/blob/master/assets/lsb.png)

------

### [LSB: A Lightweight Scalable BlockChain for IoT Security and Privacy](https://arxiv.org/pdf/1712.02969.pdf)

**Abstract**

_Ez a tanulmány egy korábbi publikációban bemutatott lightweight BC rendszert tár fel smart home környezetben. A 
rendszer három fő komponensből áll: cloud storage, overlay, smart home. Minden smart home egy speciális komponenssel van
felszerelve, mely egy alacsony down-time, magas erőforrású eszköz. Erre a tanulmányban "miner"-ként hivatkoznak, és 
feladata egy privát blockchain fenntartása és tranzakciók, valamint bármilyen jellegű ki- és bemenő kommunikáció auditálása._

------

#### Központi komponensek

- **Tranzakciók**

Lokális eszközök közti kommunikáció, melyek több típusba sorolhatóak. *Store* tranzakciót az eszközök generálnak
adattárolás céljával. *Acces* tranzakciót az SP vagy tulajdonos küldhet, hogy eléjre a felhő tárhelyet.
*Minotor* tranzakció a tulajdonostól érkezhet, mellyel preiodikusan vizsgálja az eszközök állapotát. *Genesis* új 
eszközt ad a hálózathoz, melyet később a *remove* tranzakcióval távolíthatunk el. Az említett utasításokat a lokális
privát blockchain tárolja.

- **Lokális blockchain**

Ez a komponens tárolja a tranzkaciókat és azok sorrendjét, valamint rendelkezik egy policy header-rel, mely rákényszeríti 
a policy betartását a felhasználók ki- és bemenő tranzakcióira.

- **Miner**

Miner egy olyan eszköz, mely központilag dolgozza fel a bejövő és kimenő forgalmat. A miner integrálódhat
az otthon internet gateway-be, vagy lehet egy külön eszköz. Hasonlóan a jelenlegi központi 
biztonsági eszközökhöz, a miner autorizálja, autentikálja, valamint auditálja a tranzakciókat.

- **Lokális tár**

Lokális tár egy fizikális tároló eszköz, mely egy FIFO-ban tárolja az eszközök adatait.

#### Blockchain alapú smart home

- **Inicializáció**

Ez a folyamat, mely során új eszköz kapcsolódik a smart home-hoz. Először a miner egy kulcspárt generál
általánosított Diffie-Hellman módszerrel. A kulcs a genesis tranzakcióban kerül tárolásra.

------

## Economy

### [Internet of Things, Blockchain and Shared Economy Applications](https://www.researchgate.net/publication/308484029_Internet_of_Things_Blockchain_and_Shared_Economy_Applications)

**Abstract**

_**Ez a tanulmány fényt derít a Blockchain számára eddig kiaknázatlan területkre a megosztott gazdasági 
alkalmazásokban.** Megosztott gazdasági alkalmazások például az Uber és Airbnb, de ezek mellett még rengeteg
lehetőség van digitális vagyon megosztására._

------

#### ExpressIT

ExpressIT egy prototípus IoT alkalmazás, mely több IoT modult foglal magában.

- SmartBasket (okos bevásárlókosár)
- AutoPay (jármű alapú fizetési rendszer)
- BillSplitter (számla megosztó rendszer)
- Rewards Gateway and Payment Gateway (jutalmi gateway)

És egyéb tervezési fikció prototípusok,

- Journey Planner
- Driver Monitor
- Journey Monitor

ExpressIT valós objektumokkal kommunikál, mely hagyományos esetben emberi közreműködést
igényelne. Míg ez a rendszer bejárja az IoT nyújtotta koncepciók nagy részét, nem hasznosítja
a Blockchain nyújtotta technógiai előnyöket.

A tanulmány részletes esetleírásban ismerteti az ExpressIT, azon belül az AutoPay működését.

![autopay](https://github.com/Mrpatekful/iot-projects/blob/master/assets/autopay.png)

------

## Projects & applications

### [Decentralized vehicle rental system](https://github.com/blockchain-IoT/Motoro)
(Smart city)

**Description**

------

**Technologies**

------


### [Blockchain for connected home by Comcast](https://labs.comcast.com/blockchain-for-the-connected-home-combining-security-and-flexibility)
(Smart home)

**Description**

------

**Technologies**

------


### [Samsun IOT.js](https://github.com/Samsung/iotjs)
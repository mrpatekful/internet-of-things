# Internet of Things related articles and know-hows

## Contents

- [Big Data](https://github.com/Mrpatekful/iot-projects/tree/master/bigdata)

- [Blockchain](https://github.com/Mrpatekful/iot-projects/tree/master/blockchain)

- [Thingworx](https://github.com/Mrpatekful/iot-projects/tree/master/thingworx)

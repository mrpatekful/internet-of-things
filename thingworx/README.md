# Tracking GPS sensors in Thingworx platform

## Components

### BuildingTemplate

 `BuildingTemplate` is a `ThingTemplate`, which is implemented by a `Thing` that represents the technological center building. This component has a [service](https://support.ptc.com/cs/help/thingworx_hc/thingworx_6.5_hc/index.jspx?id=ThingServices&action=show), that querries information from the sensor tag [api](https://bme-i.sunstone-cloud.com/tagdata) by calling the `refresh` service provided by each `Tag` entity. This service also triggers the `refresh` service of `Lamp` things.

#### Building properties

- **`dataUrl`** *( Stores the url for the data API. )*

#### Building services

- **`Refresh`** *( Calls the `refresh` service of each `Tag` and `Lamp` thing. )*

------

### Building

The `Thing` `Building` instantiates the `BuildingTemplate` base class.

#### Building instances

There is only a single instance of `BuildingTemplate`, which is called `Building`.

------

### RefresherTemplate

The `BaseThingTemplate` for this component is a `Timer`, because it provides a `Timer event`, that fires at a given time interval. For more information and best practices on `Timer` see this [link](https://community.ptc.com/t5/IoT-Tech-Tips/Timers-and-Schedulers-Best-Practices/td-p/537812).

### Refresher

#### Refresher subscriptions

- **`TimerTriggered`** *( Triggered by `TimerEvent` and calls `Building`'s `Refresh` service. )*

------

### TagTemplate

`TagTemplate` is the base template for the `Tag` instances in the model. Tags are Thingworx model representations of the GPS sensors.
`TagTemplate` defines the properties of the sensor entities, which consist of an `x` and `y` coordinate. The range of `x` values falls between 0 and -15. `y` values range from 0 to -6.

#### Tag properties

- **`x`** *( Stores the x axis coordinate of the `Tag` position. )*  

- **`y`** *( Stores the y axis coordinate of the `Tag` position. )*

- **`id`** *( Stored the identifier of the `Tag` )*

#### Tag services

- **`Refresh`** *( Querries the location from bme-i.sunstone-cloud api and modifies the `x` and `y` properties. )*

------

### Tag

### Tag Instances

There are 5 tags, which have to be tracked inside the building, thus there are 5 `Thing` components for their representation in the Thingworx framework. The name of these `Thing`s are the following: `Tag1`, ... , `Tag4`, `Tag5`.

------

### LampTemplate

`LampTemplate` is the `BaseThingTemplate` for the lamp instances. Their responsibility is to keep track of the nearby GPU sensors or `Tag` things as they are represented in Thingworx. `LampTemplate` provide a `refresh` method, which calculates the distance for each `Tag` to the particular `Lamp` instance, and stores the `id` of the `Tag` and `distance` parameters in an `InfoTable` type property called `nearbyTags`.

#### Lamp properties

- **`nearbyTags`** *( InfoTable that holds the data of nearby tags. )*
- **`x`** *( Stores the x axis coordinate of the `Lamp` position. )*
- **`y`** *( Stores the x axis coordinate of the `Lamp` position. )*
- **`radius`** *( The radius, which marks the proximity threshold for the nearby `Tag`s. )*
- **`color`** *( The color of the tag. )*
- **`url`** *( )*
- **`prev_color`** *()*

#### Lamp services

- **`CalculateDistance`** *(Given and `x` and `y` corrdinate, calculates the distance. )*
- **`Refresh`** *( Calculates the euclidean distance of each `Tag` to the `Lamp` instance, and stores those, which are closer than `radius`. )*

------

### Lamp

#### Lamp Instances

The technological center site is divided up to 8 segments, thus 8 lamps have to be modelled. These are called `Lamp1`, `Lamp2`, ... , `Lamp7`, `Lamp8`.

------

### TagDistanceDataShape

This `DataShape` contains the `id` and `distance` of the `Tag`.

#### TagDistance properties

- **`id`** *( Identifier of the `Tag` thing. )*
- **`distance`** *( Distance of the tag to the lamp. )*

------

## Implementation

`BuildingTemplate` ha a single property `dataUrl`, which contains the *https://bme-i.sunstone-cloud.com/tagdata* address. This value field is created as **persistent**.

The `BuildingTemplate` provides a service called `refresh` and a subscription called `timerTriggered`, which are specified in the previous section. The implementation of these methods are shown in the following code snippets.

*`BuildingTemplate`*.**`Refresh`**

```javascript
var tags = ThingTemplates["TagTemplate"].GetImplementingThingsWithData();

for (var i = 0; i < tags.rows.length; i++) {
    var tag = Things[tags.rows[i].name];
    tag.Refresh({dataUrl: me.dataUrl});
}

var lamps = ThingTemplates["LampTemplate"].GetImplementingThingsWithData();

for (var j = 0; j < tags.rows.length; j++) {
    var lamp = Things[lamps.rows[j].name];
    lamp.Refresh();
}
```

------

*`RefresherTemplate`*.**`TimerTriggered`**

```javascript
me.Refresh();
```

------

The properties of `TagTemplate` are the two location coordinates, `x`, `y` which are `NUMBER` types and the `id`, which is a `TEXT` type field. Each of these properties are created as **persistent**.

`TagTemplate` also provides a service called `Refresh`, which is called by `BuildingTemplate`'s `Refresh` service and performs the information retrieval through REST API call towards the data endpoint. The method has an input parameter called `dataUrl`, which is extended with the `Tag`'s `id` to retrieve the location data.

*`TagTemplate`*.**`Refresh`**

```javascript
var params = {
    proxyScheme: undefined,
    headers: undefined,
    ignoreSSLErrors: undefined,
    useNTLM: undefined,
    workstation: undefined,
    useProxy: undefined,
    withCookies: undefined,
    proxyHost: undefined,
    url: dataUrl + "/" + me.id,
    timeout: undefined,
    proxyPort: undefined,
    password: undefined,
    domain: undefined,
    username: undefined
};

var result = Resources["ContentLoaderFunctions"].GetJSON(params);

me.x = result["pos"][0];
me.y = result["pos"][1];
```

------

The properties of `LampTemplate` are a `nearbyTags` `InfoTable`, that stores element defined by `TagDistanceDataShape`, the `x`, `y` location coordinates and the `radius`. The values of `x`, `y` and `radius` are persistent, and the latter also has a default value of an aribarily chosen number.

`LampTemplate` has a `CalculateDistance` service, which by providing an `x` and `y` coordinate computes the distance to the `Lamp` `Thing`. Similarly to the previous components, `LampTemplate` also has a `Refresh` service, which uses `CaldulateDistance` service to compute the distance to each `Tag`. The `Tag`s which's resulting distance fall inside the distance threshold, defined by the `radius` property, are added to the `nearbyTags` `InfoTable`.

*`LampTemplate`*.**`CalculateDistance`**

```javascript
var result = Math.sqrt(Math.pow(x - me.x, 2) + Math.pow(y - me.y, 2));
```

*`LampTemplate`*.**`Refresh`**

```javascript
var tags = ThingTemplates["TagTemplate"].GetImplementingThingsWithData();

me.nearbyTags.RemoveAllRows();

for (var i = 0; i < tags.rows.length; i++) {
    var name = tags.rows[i].name;
    var tag = Things[name];
    var distance = me.CalculateDistance({x: tag.x, y: tag.y});
    var result = 1;
    if (distance < me.radius){
        me.nearbyTags.AddRow({id: tag.id, name: name, distance: distance});
    }
}

me.SendTagData();
```

*`LampTemplate`*.**`SendTagData`**

```javascript
var tags = ThingTemplates["TagTemplate"].GetImplementingThingsWithData();

var sort = new Object();
sort.name = "distance";
sort.ascending = true;
me.nearbyTags.Sort(sort);

var color;

if (me.nearbyTags.rows.length > 0) {
    var row = me.nearbyTags.rows[0];
    var tag = tags.Find({name: row.name});
    color = tag.color;

} else {
    color = "255_255_255";
}

var url = me.url + "/" + me.id + "/" + color;

if (color != me.prev_color){
    me.prev_color = color;
    var params = {
        proxyScheme: undefined /* STRING */,
        headers: undefined /* JSON */,
        ignoreSSLErrors: undefined /* BOOLEAN */,
        useNTLM: undefined /* BOOLEAN */,
        workstation: undefined /* STRING */,
        useProxy: undefined /* BOOLEAN */,
        withCookies: undefined /* BOOLEAN */,
        proxyHost: undefined /* STRING */,
        url: url /* STRING */,
        content: undefined /* JSON */,
        timeout: undefined /* NUMBER */,
        proxyPort: undefined /* INTEGER */,
        password: undefined /* STRING */,
        domain: undefined /* STRING */,
        username: undefined /* STRING */
    };

    // result: JSON
    var response = Resources["ContentLoaderFunctions"].GetJSON(params);
}
```

### Mashup

There are 8 List tpye containers, which represent the list for the nearby tags of each lamp (`nearbyTags` infotable). (Note: to visualize the values of the infotable in the list, set the display value property at the list mashup properties window to `id`). The `Data` fields of the lists are connected to the `nearbyTags` property, which is fetched by the `GetPropertyValues` service. The `AutoRefresh` mashup entity is used to periodically refresh the lists by calling the `GetPropertyValues` service. It also calls the `Refresh` service of the `Building` thing. The background image is fetched from an URL, extracted from *bme-i.sunstone-cloud.com/livemap*.

#### Overview

![overview](images/mashup.png)

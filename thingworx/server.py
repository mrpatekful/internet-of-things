
from flask import Flask
from flask import request

app = Flask(__name__)


@app.route('/<lamp_id>/<color>')
def home(lamp_id, color):
    # color -> R_G_B
    print(lamp_id, color.split('_'))
    return ''


if __name__ == '__main__':
    app.run(host='0.0.0.0')

# Big data in the Internet of Things

- [Deep Learning for IoT Big Data and Streaming Analytics: A Survey](https://arxiv.org/abs/1712.04301)

- [Evolvable Systems for Big Data Management in Business](https://arxiv.org/abs/1803.07434)

- [Big Data Analytics and Its Applications](https://arxiv.org/abs/1710.04135)

- [Industrial Big Data Analytics: Challenges, Methodologies, and Applications](https://arxiv.org/abs/1807.01016)